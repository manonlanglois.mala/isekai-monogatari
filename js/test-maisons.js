const skillHouseMapping = {

  // Intellect
  // Logique
  skill1: ["Fugi", "Suisei", "Hiisa"],
  // Culture générale
  skill2: ["Fugi", "Raiyo", "Dojin"],
  // Rhétorique
  skill3: ["Raiyo", "Suisei", "Fugi"],
  // Théâtralité
  skill4: ["Raiyo", "Suisei", "Dojin"],
  // Conceptualisation
  skill5: ["Dojin", "Raiyo"],

  // Psyché
  // Volonté
  skill6: ["Fugi", "Dojin"],
  // Spiritualité
  skill7: ["Fugi", "Dojin"],
  // Empathie
  skill8: ["Raiyo", "Suisei", "Dojin"],
  // Autorité
  skill9: ["Fugi", "Hiisa"],
  // Suggestion
  skill10: ["Hiisa", "Raiyo", "Suisei"],

  // Physique
  // Endurance
  skill11: ["Hiisa", "Suisei"],
  // Résistance
  skill12: ["Hiisa", "Fugi"],
  // Force physique
  skill13: ["Hiisa", "Raiyo"],
  // Électrochimie
  skill14: ["Raiyo", "Hiisa", "Dojin"],
  // Instinct de Survie
  skill15: ["Suisei", "Hiisa", "Dojin"],

  // Motricité
  // Coordination
  skill16: ["Raiyo", "Fugi", "Hiisa"],
  // Perception
  skill17: ["Fugi", "Dojin"],
  // Réactivité
  skill18: ["Raiyo", "Suisei", "Hiisa"],
  // Discrétion
  skill19: ["Dojin", "Suisei"],
  // Contrôle
  skill20: ["Fugi", "Suisei"]
};

const mbtiHouseMapping = {
  // Green
  INFJ: { primary: ["Dojin"], secondary: ["Fugi", "Suisei"] },
  ENFJ: { primary: ["Dojin"], secondary: ["Fugi", "Raiyo"] },
  INFP: { primary: ["Dojin"], secondary: ["Raiyo", "Suisei"] },
  ENFP: { primary: ["Dojin"], secondary: ["Hiisa", "Raiyo"] },

  // Blue
  ISFJ: { primary: ["Dojin"], secondary: ["Fugi", "Suisei"] },
  ESFJ: { primary: ["Fugi"], secondary: ["Dojin", "Raiyo"] },
  ISFP: { primary: ["Hiisa", "Raiyo"], secondary: ["Dojin"] },
  ESFP: { primary: ["Raiyo"], secondary: ["Dojin", "Hiisa"] },

  //Violet
  INTP: { primary: ["Suisei"], secondary: ["Fugi", "Hiisa"] },
  ENTP: { primary: ["Raiyo"], secondary: ["Hiisa", "Suisei"] },
  INTJ: { primary: ["Fugi"], secondary: ["Hiisa", "Suisei"] },
  ENTJ: { primary: ["Hiisa"], secondary: ["Fugi", "Suisei"] },

  // Yellow
  ISTP: { primary: ["Hiisa"], secondary: ["Raiyo"] },
  ESTP: { primary: ["Raiyo"], secondary: ["Hiisa", "Suisei"] },
  ISTJ: { primary: ["Fugi"], secondary: ["Suisei"] },
  ESTJ: { primary: ["Fugi"], secondary: ["Hiisa", "Suisei"] }
};

function calculateScore() {
  let totalScore = {};
  for (const skill in skillHouseMapping) {
    const skillValue = parseInt(document.querySelector(`input[name=${skill}]:checked`).value);
    skillHouseMapping[skill].forEach(house => {
      if (totalScore[house]) {
        totalScore[house] += skillValue;
      } else {
        totalScore[house] = skillValue;
      }
    });
  }
  // const selectedMbti = document.getElementById("mbti").value;
  const selectedMbti = document.querySelector(`input[name="MBTI"]:checked`).value;
  const mbtiBonusHouses = mbtiHouseMapping[selectedMbti].primary;
  const mbtiSecondaryHouses = mbtiHouseMapping[selectedMbti].secondary;
  for (const house in totalScore) {
    if (mbtiBonusHouses.includes(house)) {
      totalScore[house] += 10;
    } else if (mbtiSecondaryHouses.includes(house)) {
      totalScore[house] += 5;
    }
  }
  return totalScore;
}

// function displayResult() {
  // const resultDiv = document.getElementById("result");
  // resultDiv.innerHTML = "";
  // const totalScore = calculateScore();
  // const sortedHouses = Object.keys(totalScore).sort((a, b) => totalScore[b] - totalScore[a]);
  // const winningHouse = sortedHouses[0];
  // const winningScore = totalScore[winningHouse];
  // const resultText = `You belong to ${winningHouse} with a score of ${winningScore}`;
  // const resultNode = document.createTextNode(resultText);
  // resultDiv.appendChild(resultNode);
// }

const houseDescriptionMapping = {
  "Dojin": {
    intro: "<span>Tu es fait∙e pour la maison</span><strong>Dojin</strong>",
    description: "<strong>Compassion, stabilité et entêtement</strong><p>Les Mages de la maison Dojin vivent de leurs <strong>sentiments</strong>, racine même de leur <strong>force de caractère</strong>. Cœur sur la main, iels sont <em>profondément</em> connecté∙e∙s avec leur facette <strong>émotionnelle</strong>, leur donnant un immense potentiel de <strong>stabilité psychique</strong>. Mais ce lien privilégié avec leurs émotions peut s’avérer un <em>obstacle</em> en société, alors qu’il est <strong>difficile</strong> pour les Dojin-ka de faire la part des choses lorsque leur <strong>ressenti</strong> se retrouve parfois <em>dénigré</em> ou <em>mis de côté</em> au profit d’autre chose. Ils peuvent se montrer très <strong>entêté∙e∙s</strong>, envahissant leur naturel pourtant <strong>communicatif</strong> pour <strong>bloquer la discussion</strong> et la résolution des conflits à leur éclosion.</p>",
    exemples: "<div class=\"exemple\"><img src=\"https://i.imgur.com/JDrrOhZ.png\" /><div><strong>Princesse Zelda</strong><p>The Legend of Zelda</p></div></div><div class=\"exemple\"><img src=\"https://i.imgur.com/tdsOEiF.png\" /><div><strong>Retsuko</strong><p>Aggretsuko</p></div></div><div class=\"exemple\"><img src=\"https://i.imgur.com/repGbn9.png\" /><div><strong>Bran Stark</strong><p>Game of Thrones</p></div></div>"
  },
  "Fugi": {
    intro: "<span>Tu es fait∙e pour la maison</span><strong>Fugi</strong>",
    description: "<strong>Droiture, sagesse et esquive</strong><p>Forces tranquilles de l’archipel, les Fugi-ka sont perçu∙e∙s comme des <strong>représentant∙e∙s de la justice</strong>. Leur vision de l’<strong>égalité</strong> est leur priorité, et iels se fichent assez de l’<strong>image</strong> qu’iels peuvent renvoyer ; certains Fugi-ka sont décrit∙e∙s comme <strong>froid∙e∙s</strong> et <strong>impassibles</strong>, d’autres <strong>direct∙e∙s</strong> et <strong>tranchant∙e∙s</strong>. Iels sont particulièrement doué∙e∙s pour <em>peser le pour et le contre</em> dans le calme, et prendre les décisions qui leur semblent les plus <strong>raisonnables</strong>, quelle que soit leur définition du terme. Mais ce <em>mur de marbre</em> et de droiture peut parfois aussi leur donner l’air de se cacher derrière les <em>faits</em> ou des <em>excuses</em> sans trop se dévoiler. Il est parfois <strong>difficile de cerner</strong> un∙e Fugi-ka, qui semble toujours vous <em>filer entre les doigts</em>.</p>",
    exemples: "<div class=\"exemple\"><img src=\"https://i.imgur.com/3OsN0eb.png\" /><div><strong>Hershel Layton</strong><p>Professeur Layton</p></div></div><div class=\"exemple\"><img src=\"https://i.imgur.com/BO6oQgn.png\" /><div><strong>Wednesday Addams</strong><p>Wednesday</p></div></div><div class=\"exemple\"><img src=\"https://i.imgur.com/vZPLW3y.png\" /><div><strong>Usagi Yuzuha</strong><p>Alice in Borderland</p></div></div>"
  },
  "Hiisa": {
    intro: "<span>Tu es fait∙e pour la maison</span><strong>Hiisa</strong>",
    description: "<strong>Courage, énergie et intimidation</strong><p>La maison Hiisa est réputée pour ses <em>figures d’influence</em> <strong>charismatiques</strong> et ses <strong>têtes brûlées</strong>. Mais leur définition de courage est à multiples facettes ; les Hiisa-ka croient au courage de savoir faire <strong>ce qui est juste quand il le faut</strong>, sans jamais douter. Énergiques, iels vont de l’avant, avec <strong>confiance</strong> et <strong>détermination</strong> - mais si <em>faire demi-tour</em> est la bonne chose à faire, iels en prendront la <em>responsabilité</em>. Mais ce courage va aussi avec une <strong>témérité</strong> parfois <strong>irréfléchie</strong>, et une <strong>rage de vaincre</strong> et d’avancer vers leurs objectifs qui peut prendre au cœur ; iels n’ont pas peur de la <strong>confrontation</strong> ni de <em>monter le ton</em>, pouvant parfois passer pour des tentatives d’<strong>intimidation</strong>.</p>",
    exemples: "<div class=\"exemple\"><img src=\"https://i.imgur.com/JO3S2xt.png\" /><div><strong>Ellie Williams</strong><p>The Last of Us</p></div></div><div class=\"exemple\"><img src=\"https://i.imgur.com/2Jt1JrH.png\" /><div><strong>Sohma Kyō</strong><p>Fruits Basket</p></div></div><div class=\"exemple\"><img src=\"https://i.imgur.com/Ph34sV5.png\" /><div><strong>Luz Noceda</strong><p>The Owl House</p></div></div>"
  },
  "Raiyo": {
    intro: "<span>Tu es fait∙e pour la maison</span><strong>Raiyo</strong>",
    description: "<strong>Honneur, créativité et imprévisibilité</strong><p>Les Raiyo-ka sont des <strong>électrons libres</strong>, forces de <strong>créativité</strong> et maître∙sse∙s de l’<strong>improvisation</strong> à toute épreuve. Souvent <strong>optimistes</strong>, ce naturel <strong>volatile</strong> est pourtant <em>profondément</em> dirigé par leur sens de la <strong>loyauté</strong> ; tous les moyens sont bons pour <em>exprimer leur affection</em> et leur <em>respect</em> envers l’objet de leur fidélité. Les Raiyo-ka sont cependant parfois <strong>imprévisibles</strong> et <strong>éparpillé∙e∙s</strong>. Iels peuvent facilement se trouver <strong>distrait∙e∙s</strong> de leur but premier par le <em>champ des possibles</em> qui s’offre à elleux, leur donnant une image parfois <strong>irresponsable</strong> <em>(à tort ou à raison)</em>.</p>",
    exemples: "<div class=\"exemple\"><img src=\"https://i.imgur.com/O6DxXxV.png\" /><div><strong>Hange Zoe</strong><p>Shingeki no Kyōjin</p></div></div><div class=\"exemple\"><img src=\"https://i.imgur.com/GywfUzA.png\" /><div><strong>Wei Wuxian</strong><p>The Untamed</p></div></div><div class=\"exemple\"><img src=\"https://i.imgur.com/uRfWVW1.png\" /><div><strong>Max Mayfield</strong><p>Stranger Things</p></div></div>"
  },
  "Suisei": {
    intro: "<span>Tu es fait∙e pour la maison</span><strong>Suisei</strong>",
    description: "<strong>Sincérité, flexibilité et manipulation</strong><p>Les Mages de la maison Suisei sont <strong>vrai∙e∙s</strong>, <strong>honnêtes</strong> et <strong>sincères</strong>. Iels portent leurs <strong>pensées</strong> et leurs <strong>émotions</strong> sur leur corps, <em>même sans les verbaliser</em>. <strong>Sensibles</strong> et fortement <em>dérangé∙e∙s par les mensonges</em> et les coups en douce, les Suisei-ka sont de <strong>piètres menteur∙se∙s</strong>, et font preuve d’une <strong>souplesse</strong> sans précédent pour compenser. <strong>Maître∙sse∙s des mots</strong>, iels savent <em>jongler avec la réalité</em> pour <strong>accommoder</strong> autrui, mais surtout <em>elleux-mêmes</em> ; ce bagout peut parfois passer pour de la <strong>manipulation</strong>, bien que toujours mué par une <strong>vérité essentielle</strong> que tout le monde ne voit pas toujours.</p>",
    exemples: "<div class=\"exemple\"><img src=\"https://i.imgur.com/dqlHLEg.png\" /><div><strong>Five Hargreeves</strong><p>Umbrella Academy</p></div></div><div class=\"exemple\"><img src=\"https://i.imgur.com/NTixqOJ.png\" /><div><strong>Traveller</strong><p>Genshin Impact</p></div></div><div class=\"exemple\"><img src=\"https://i.imgur.com/u1NEY5V.png\" /><div><strong>Cho Sang-woo</strong><p>Squid Game</p></div></div>"
  }
};

function displayResult() {
  const resultDiv = document.getElementById("result");
  const resultContainer = document.getElementById("result-container");
  const exemplesContainer = document.getElementById("exemples-container");
  const scoreDiv = document.getElementById("score");

  resultDiv.innerHTML = "";
  const totalScore = calculateScore();
  const sortedHouses = Object.keys(totalScore).sort((a, b) => totalScore[b] - totalScore[a]);

  const winningHouse = sortedHouses[0];
  const winningScore = totalScore[winningHouse];
  const secondHouse = sortedHouses[1];
  const secondScore = totalScore[secondHouse];
  const thirdHouse = sortedHouses[2];
  const thirdScore = totalScore[thirdHouse];
  const fourthHouse = sortedHouses[3];
  const fourthScore = totalScore[fourthHouse];
  const fifthHouse = sortedHouses[4];
  const fifthScore = totalScore[fifthHouse];

  // const resultText = `Tu es un.e mage fait.e pour la maison ${winningHouse} !`;
  // const introNode = document.createElement("div.intro");
  // const resultNode = document.createTextNode(resultText);
  // introNode.textContent = resultText.description;
  // resultDiv.appendChild(resultNode);
  

  // Contenu sur la maison
  const houseDescription = houseDescriptionMapping[winningHouse];
  resultContainer.className = `${winningHouse}`;

  // Scores
  const scoreNode = document.createElement("div");
  scoreNode.className = "score";
  scoreNode.innerHTML = `<div class=\"score-item score-${winningHouse}\"><strong>${winningScore}</strong><span>${winningHouse}</span></div><div class=\"score-item score-${secondHouse}\"><strong>${secondScore}</strong><span>${secondHouse}</span></div><div class=\"score-item score-${thirdHouse}\"><strong>${thirdScore}</strong><span>${thirdHouse}</span></div><div class=\"score-item score-${fourthHouse}\"><strong>${fourthScore}</strong><span>${fourthHouse}</span></div><div class=\"score-item score-${fifthHouse}\"><strong>${fifthScore}</strong><span>${fifthHouse}</span></div>`;
  resultDiv.appendChild(scoreNode);

  const introNode = document.createElement("div");
  introNode.className = "intro";
  introNode.innerHTML = houseDescription.intro;
  resultDiv.appendChild(introNode);

  const descriptionNode = document.createElement("div");
  descriptionNode.className = "description";
  descriptionNode.innerHTML = houseDescription.description;
  resultDiv.appendChild(descriptionNode);

  // Exemples
  const exempleNode = document.createElement("div");
  exempleNode.className = "exemples-container";
  exempleNode.innerHTML = houseDescription.exemples;
  resultDiv.appendChild(exempleNode);
 
  // Toggle classes pour afficher la fenêtre
  resultContainer.classList.add("show-results");
  document.body.classList.toggle("stop-scrolling");
}

document.getElementById("quiz-form").addEventListener("submit", event => {
  event.preventDefault();
  displayResult();
});
